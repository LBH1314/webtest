<%--
  Created by IntelliJ IDEA.
  User: asus
  Date: 2019/4/11
  Time: 8:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>角色添加</title>
</head>
<body>
    <form name="fromRole" action="${pageContext.request.contextPath}/roleAdd" method="post" >
        <table>
        <table width="60%" border="0" align="center">
            <tr>
                <td>角色添加</td>
            </tr>
                <tr>
                <td align="right">角色名称：</td>
                <td>
                    <input
                           name="roleName" type="text"/>
                </td>
            </tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr>
                <td align="right">角色代码：</td>
                <td><input
                           name="roleCode" type="text" /></td>
            </tr>

        </table>
        <table width="60%" border="0" align="center">
            <tr></tr><tr></tr><tr></tr>
            <%--<tr>--%>
            <%--<td align="left">已分配权限：</td>--%>
            <%--<td align="center">         </td>--%>
            <%--<td align="right">可分配权限：</td>--%>
            <%--</tr>--%>
            <tr>
                <td width="60" align="center">
                    <%--<select  name="quanxian" style="height: 200px;width: 200px"></select>--%>
                    <select class="select2_multiple form-control" multiple="multiple"
                            size="15" id="rightId" name="rightId">
                        <option value="-100">已分配权限</option>
                    </select>
                </td>
                <td width="60" align="center">
                    <input type="button" name="btntianjia" value="添加" onclick="btntianjia()">
                    <input type="button" name="btnyichu" value="移除" onclick="btnyichu()">

                </td>
                <td width="60" align="center">

                    <select class="select2_multiple form-control" multiple="multiple"
                            size="15" id="rightId2" name="rightId2">
                        <option value="-100">可分配权限</option>
                        <c:forEach var="moduel" items="${moduellist}">


                            <option value="${moduel.moduelnum}">${moduel.appnum}.${moduel.moduelname}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <input type="submit" value="保存"onclick="btnAddSave()"/>
                    <input type="reset" value="重置"/>&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="button" value="取消" onclick="btncancel()"/>
                </td>
            </tr>
        </table>
        </table>
    </form>
</fieldset>

<script language="JavaScript">
    function btnAddSave() {
        document.fromRole.action = "/roleAdd";
        document.fromRole.submit();
    }
    function btncancel() {
        document.location.replace("/roleList");
    }

</script>
</body>
</html>
