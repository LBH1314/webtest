<%--
  Created by IntelliJ IDEA.
  User: asus
  Date: 2019/4/11
  Time: 8:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>角色修改</title>
</head>
<body>
<form name="frmUser" action="${pageContext.request.contextPath}/roleEdit" method="post" >
    <input type="hidden" name="roleId" value="${role.roleId}">
    <table border="1" width="90%" aglin="center">
        <tr>
            <th>修改角色</th>
        </tr>
        <tr>
            <td>
                <table border="1" width="100%" aglin="center">
                    <tr>
                        <td >角色名：</td>
                        <td >

                            <input name="roleName" type="text" size="30"
                                   placeholder="角色1" value="${role.roleName}">

                        </td>
                    </tr>


                    <tr>
                        <td>角色代码：</td>
                        <td>

                            <input name="roleCode" type="text" size="30"
                                   placeholder="招聘" value="${role.roleCode}">

                        </td>
                    </tr>

                    <tr>
                        <td>应用：</td>
                        <td>

                            <input name="roleDesc" type="text" size="30"
                                   placeholder="16001" value="${role.roleDesc}">

                        </td>
                    </tr>






                </table>
            </td>
        </tr>


        <tr>
            <th>
                <input name="btnAdd" type="button" value="保存" onclick="useradd()">
                <input name="btnAReset" type="reset" value="重置" >
                <input name="btnCancel" type="button" value="退出" onclick="userCal()">
            </th>
        </tr>


    </table>

</form>
<script language="JavaScript">
    function useradd() {
        document.frmUser.action = "/roleEdit";
        document.frmUser.submit();
    }
    function userCal() {
        document.location.replace("/roleList");
    }
</script>


</body>
</html>
