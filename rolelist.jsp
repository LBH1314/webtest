<%--
  Created by IntelliJ IDEA.
  User: asus
  Date: 2019/4/11
  Time: 8:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="UTF-8">
    <title>角色列表</title>
</head>
<body>
<form name="fromRole" action="#" method="post">
    <table width="60%" border="0" align="center">
        <tr>
            <td align="left" >

                <input type="button" name="btnadd"
                       value="添加角色" onclick="roleadd()">
            </td>
        </tr>
    </table>

    <table width="60%" border="1" align="center">

        <tr >
            <td >序号</td>
            <td>角色名</td>
            <td>角色代码</td>
            <td>应用</td>
        </tr>


        <c:forEach var="role" items="${rolelist}" varStatus="vstatus">

            <tr >
                <td align="center">
                    <input type="radio" name="roleId" value="${role.roleId}">
                </td>
                <td>${role.roleName}</td>
                <td>${role.roleCode}</td>
                <td>${role.roleDesc}</td>



            </tr>


        </c:forEach>

    </table>
    </div>
    </div>




    <table width="60%" border="0" align="center">
        <tr>
            <td align="left" >


                <input type="button" name="btnedit"
                       value="修改" onclick="roleedit()">&nbsp;&nbsp;&nbsp;
                <input type="button" name="btndel"
                       value="删除" onclick="roledel()">&nbsp;&nbsp;&nbsp;
                <input type="button" name="btncon"
                       value="退出" onclick="rolecon()">

            </td>
        </tr>
    </table>
</form>
<script language="JavaScript">
    function roleadd() {
        window.location.replace("${pageContext.request.contextPath}/toRoleAdd");
    }

    function roleedit() {

        var roleids=document.getElementsByName("roleId");
        var isSelected=false;
        for(i=0;i<roleids.length;i++){
            if(roleids[i].checked){
                isSelected=true;
            }
        }
        if(isSelected){
            document.fromRole.action="/toRoleEdit"
            document.fromRole.submit();
        }else{
            alert("请先选择角色！");
        }
    }

    function roledel() {
        var roleids=document.getElementsByName("roleId");
        var isSelected=false;
        //循环判断是否有选中的角色
        for(i=0;i<roleids.length;i++){
            if(roleids[i].checked){
                isSelected=true;
            }
        }
        if(isSelected){
            //如果选择了角色
            if(confirm("确认删除选择的角色吗？")){
                document.fromRole.action="toRoleDel";
                document.fromRole.submit();
            }
        }else{
            //否则进行提示
            alert("请先选择角色！");
        }
    }



</script>


</body>
</html>
